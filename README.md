# pancakes -- PWN -- TUCTF2019

[Source](https://gitlab.com/tuctf2019/pwn/pancakes)

## Chal Info

Desc: `You ever just get a craving for pancakes?`

Given files:

* pancakes

Hints:

* Where is the flag when it's read?

Flag: `TUCTF{p4nc4k35_4r3_4b50lu73ly_d3l1c10u5_4nd_y0u_5h0uld_637_50m3_4f73r_7h15}`

## Deployment

[Docker Hub](https://hub.docker.com/r/asciioverflow/pancakes)

Ports: 8888

Example usage:

```bash
docker run -d -p 127.0.0.1:8888:8888 asciioverflow/pancakes:tuctf2019
```
